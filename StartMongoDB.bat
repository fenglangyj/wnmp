@echo off

set MONGODB_PATH=%~dp0MongoDB

echo Starting MongoDB...
bin\RunHiddenConsole "%MONGODB_PATH%\Server\3.4\bin\mongod.exe" -f "%MONGODB_PATH%\etc\mongod.conf"
REM "%MONGODB_PATH%\Server\3.4\bin\mongod.exe" -f "%MONGODB_PATH%\etc\mongod.conf"

pause

exit