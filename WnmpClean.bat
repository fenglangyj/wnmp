@echo off

REM 设置nginx所在目录
set WNMP_NGINX_PATH=%~dp0nginx

echo Clean nginx log file 
del "%WNMP_NGINX_PATH%\logs\access.log"
del "%WNMP_NGINX_PATH%\logs\error.log"
del "%WNMP_NGINX_PATH%\logs\nginx.pid"

pause