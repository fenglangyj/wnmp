@echo off

REM 设置php所在目录。
set WNMP_PHP_PATH=%~dp0php
REM 设置PHP CGI端口
set WNMP_PHP_PORT=9000
REM 设置nginx所在目录
set WNMP_NGINX_PATH=%~dp0nginx


echo Starting PHP FastCGI...
bin\RunHiddenConsole "%WNMP_PHP_PATH%\php-cgi.exe" -b 127.0.0.1:%WNMP_PHP_PORT% -c "%WNMP_PHP_PATH%\php.ini"
REM "%WNMP_PHP_PATH%\php-cgi.exe" -b 127.0.0.1:%WNMP_PHP_PORT% -c "%WNMP_PHP_PATH%\php.ini"
 
echo Starting nginx...
bin\RunHiddenConsole "%WNMP_NGINX_PATH%\nginx.exe" -p %WNMP_NGINX_PATH%
REM "%WNMP_NGINX_PATH%\nginx.exe" -p %WNMP_NGINX_PATH%

pause