@echo off

set WNMP_REDIS_PATH=%~dp0Redis

echo Starting Redis...
bin\RunHiddenConsole "%WNMP_REDIS_PATH%\redis-server.exe" "%WNMP_REDIS_PATH%\redis.windows.conf"

pause

exit