@echo off

REM 设置php所在目录。
set WNMP_MYSQL_PATH=%~dp0mysql-8.0.11-winx64


echo initialize MySql...

REM %WNMP_MYSQL_PATH%\bin\mysqld.exe --initialize --console
REM 初始化数据库目录（无密方式初始化） https://segmentfault.com/q/1010000014804864
%WNMP_MYSQL_PATH%\bin\mysqld.exe --initialize-insecure --user=mysql

pause